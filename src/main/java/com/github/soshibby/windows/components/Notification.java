package com.github.soshibby.windows.components;

import org.conductor.component.annotations.Property;
import org.conductor.component.types.Component;
import org.conductor.database.Database;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

/**
 * Created by Henrik on 03/12/2016.
 */
@org.conductor.component.annotations.Component(type = "notification")
public class Notification extends Component {

    private static final String POPUP_TITLE = "Conductor Message";
    private static final String TRAY_ICON_TITLE = "Conductor: Windows Notification";
    private static final String TRAY_ICON_PATH = "/images/trayIcon.png";
    private TrayIcon trayIcon;

    public Notification(Database database, Map<String, Object> options) throws AWTException {
        super(database, options);
        initializeSystemTrayIcon();
    }

    private void initializeSystemTrayIcon() throws AWTException {
        if (SystemTray.isSupported()) {
            Image image = new ImageIcon(this.getClass().getResource(TRAY_ICON_PATH)).getImage();
            trayIcon = new TrayIcon(image, TRAY_ICON_TITLE);
            SystemTray.getSystemTray().add(trayIcon);
        }
    }

    @Property(initialValue = "")
    public void setMessage(String message) {
        trayIcon.displayMessage(POPUP_TITLE, message, TrayIcon.MessageType.INFO);
    }

}

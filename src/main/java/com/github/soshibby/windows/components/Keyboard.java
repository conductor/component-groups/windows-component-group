package com.github.soshibby.windows.components;

import com.github.soshibby.windows.types.KeyListener;
import org.conductor.component.annotations.ReadOnlyProperty;
import org.conductor.component.types.Component;
import org.conductor.component.types.DataType;
import org.conductor.database.Database;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.NativeInputEvent;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

/**
 * Created by Henrik on 29/11/2016.
 */
@org.conductor.component.annotations.Component(type = "keyboard")
@ReadOnlyProperty(name = "pressed_ctrl_right+1", dataType = DataType.BOOLEAN, defaultValue = "false")
@ReadOnlyProperty(name = "pressed_ctrl_right+2", dataType = DataType.BOOLEAN, defaultValue = "false")
@ReadOnlyProperty(name = "pressed_ctrl_right+3", dataType = DataType.BOOLEAN, defaultValue = "false")
@ReadOnlyProperty(name = "pressed_ctrl_right+4", dataType = DataType.BOOLEAN, defaultValue = "false")
@ReadOnlyProperty(name = "pressed_ctrl_right+5", dataType = DataType.BOOLEAN, defaultValue = "false")
@ReadOnlyProperty(name = "pressed_ctrl_right+6", dataType = DataType.BOOLEAN, defaultValue = "false")
@ReadOnlyProperty(name = "pressed_ctrl_right+7", dataType = DataType.BOOLEAN, defaultValue = "false")
@ReadOnlyProperty(name = "pressed_ctrl_right+8", dataType = DataType.BOOLEAN, defaultValue = "false")
@ReadOnlyProperty(name = "pressed_ctrl_right+9", dataType = DataType.BOOLEAN, defaultValue = "false")
@ReadOnlyProperty(name = "pressed_ctrl_right+0", dataType = DataType.BOOLEAN, defaultValue = "false")
public class Keyboard extends Component implements NativeKeyListener {

    private static final Logger log = LoggerFactory.getLogger(Keyboard.class);
    private static List<KeyListener> keyListeners = Arrays.asList(
        new KeyListener('1', NativeInputEvent.CTRL_R_MASK),
        new KeyListener('2', NativeInputEvent.CTRL_R_MASK),
        new KeyListener('3', NativeInputEvent.CTRL_R_MASK),
        new KeyListener('4', NativeInputEvent.CTRL_R_MASK),
        new KeyListener('5', NativeInputEvent.CTRL_R_MASK),
        new KeyListener('6', NativeInputEvent.CTRL_R_MASK),
        new KeyListener('7', NativeInputEvent.CTRL_R_MASK),
        new KeyListener('8', NativeInputEvent.CTRL_R_MASK),
        new KeyListener('9', NativeInputEvent.CTRL_R_MASK),
        new KeyListener('0', NativeInputEvent.CTRL_R_MASK)
    );

    public Keyboard(Database database, Map<String, Object> options) throws NativeHookException {
        super(database, options);

        java.util.logging.Logger globalScreenLogger = java.util.logging.Logger.getLogger(GlobalScreen.class.getPackage().getName());
        globalScreenLogger.setLevel(Level.OFF);

        GlobalScreen.registerNativeHook();
        GlobalScreen.addNativeKeyListener(this);
    }

    @Override
    public void nativeKeyPressed(NativeKeyEvent event) {
        handleKeyEvent(event, true);
    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent event) {
        handleKeyEvent(event, false);
    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent nativeKeyEvent) {

    }

    private void handleKeyEvent(NativeKeyEvent event, boolean keyDown) {
        keyListeners.forEach(keyListener -> {
            if (keyListener.matches(event)) {
                try {
                    updatePropertyValue("pressed_" + keyListener.toString(), keyDown);
                } catch (Exception e) {
                    log.error("Failed to update property value.", e);
                }
            }
        });
    }

}

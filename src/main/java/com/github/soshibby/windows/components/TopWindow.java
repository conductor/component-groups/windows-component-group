package com.github.soshibby.windows.components;

import com.github.soshibby.windows.utils.WindowUtil;
import org.conductor.component.annotations.ReadOnlyProperty;
import org.conductor.component.types.Component;
import org.conductor.component.types.DataType;
import org.conductor.database.Database;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Created by Henrik on 29/11/2016.
 */
@org.conductor.component.annotations.Component(type = "userInterface")
@ReadOnlyProperty(name = "title", dataType = DataType.STRING, defaultValue = "")
@ReadOnlyProperty(name = "name", dataType = DataType.STRING, defaultValue = "")
@ReadOnlyProperty(name = "isFullscreen", dataType = DataType.BOOLEAN, defaultValue = "false")
@ReadOnlyProperty(name = "isWorkstationLocked", dataType = DataType.BOOLEAN, defaultValue = "false")
public class TopWindow extends Component implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(Keyboard.class);
    private Thread updateThread = null;

    public TopWindow(Database database, Map<String, Object> options) {
        super(database, options);
        this.updateThread = new Thread(this);
        this.updateThread.setDaemon(true);
        this.updateThread.start();
    }

    @Override
    public void run() {
        try {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    updatePropertyValue("title", WindowUtil.getTopWindowTitle());
                    updatePropertyValue("name", WindowUtil.getTopWindowName());
                    updatePropertyValue("isFullscreen", WindowUtil.isFullscreen());
                    updatePropertyValue("isWorkstationLocked", WindowUtil.isWorkstationLocked());
                } catch (Exception e) {
                    log.error("Error when updating, error was: " + e.getMessage());
                    Thread.sleep(300);
                }
            }
        } catch (InterruptedException e) {
        }
        log.warn("Stopped updating top window.");
    }

    @Override
    public void destroy() {
        updateThread.interrupt();
    }

}

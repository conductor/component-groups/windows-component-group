package com.github.soshibby.windows.types;

import org.jnativehook.NativeInputEvent;
import org.jnativehook.keyboard.NativeKeyEvent;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Henrik on 02/12/2016.
 */
public class KeyListener {

    private final char DELIMITER = '+';
    private char key;
    private int[] modifiers;
    private static Map<Integer, String> modifiersToText = new HashMap();

    static {
        modifiersToText.put(NativeInputEvent.CTRL_L_MASK, "ctrl_left");
        modifiersToText.put(NativeInputEvent.CTRL_R_MASK, "ctrl_right");
        modifiersToText.put(NativeInputEvent.SHIFT_L_MASK, "shift_left");
        modifiersToText.put(NativeInputEvent.SHIFT_R_MASK, "shift_right");
        modifiersToText.put(NativeInputEvent.ALT_L_MASK, "alt_left");
        modifiersToText.put(NativeInputEvent.ALT_R_MASK, "alt_right");
        modifiersToText.put(NativeInputEvent.META_L_MASK, "meta_left");
        modifiersToText.put(NativeInputEvent.META_R_MASK, "meta_right");
    }

    public KeyListener(char key, int... modifiers) {
        this.key = key;
        this.modifiers = modifiers;
    }

    private boolean isModifierPressed(NativeKeyEvent event, int modifier) {
        return (event.getModifiers() & modifier) != 0;
    }

    public boolean matches(NativeKeyEvent event) {
        if (event.getRawCode() != key) {
            return false;
        }

        for (int modifier : modifiers) {
            if (!isModifierPressed(event, modifier)) {
                return false;
            }
        }

        return true;
    }

    public String toString() {
        StringBuilder result = new StringBuilder();

        for (int modifier : modifiers) {
            String modifierText = modifiersToText.get(modifier);
            result.append(modifierText);
            result.append(DELIMITER);
        }

        result.append(key);
        return result.toString();
    }

}

package com.github.soshibby.windows.utils;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.PointerType;
import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.ptr.PointerByReference;
import com.sun.jna.win32.StdCallLibrary;

import java.awt.*;

/**
 * Created by Henrik on 29/11/2016.
 */
public class WindowUtil {

    private interface User32 extends StdCallLibrary {
        User32 INSTANCE = (User32) Native.loadLibrary("user32", User32.class);

        WinDef.HWND GetForegroundWindow();

        int GetWindowTextA(PointerType hWnd, byte[] lpString, int nMaxCount);

        long GetWindowThreadProcessId(PointerType hWnd, PointerByReference lpdwProcessId);
        boolean GetWindowRect(PointerType hWnd, WinDef.RECT rect);
    }

    static class Kernel32 {
        static {
            Native.register("kernel32");
        }

        public static int PROCESS_QUERY_INFORMATION = 0x0400;
        public static int PROCESS_VM_READ = 0x0010;

        public static native Pointer OpenProcess(int dwDesiredAccess, boolean bInheritHandle, Pointer pointer);
    }

    static class Psapi {
        static {
            Native.register("psapi");
        }

        public static native int GetModuleBaseNameW(Pointer hProcess, Pointer hmodule, char[] lpBaseName, int size);
    }

    public static String getTopWindowName() {
        int maxTitleLength = 1024;
        char[] buffer = new char[maxTitleLength * 2]; // create buffer
        PointerByReference pointer = new PointerByReference(); // create pointer
        WinDef.HWND foregroundWindow = User32.INSTANCE.GetForegroundWindow(); // get active window
        User32.INSTANCE.GetWindowThreadProcessId(foregroundWindow, pointer); // Get a reference to the process ID
        Pointer process = Kernel32.OpenProcess(Kernel32.PROCESS_QUERY_INFORMATION | Kernel32.PROCESS_VM_READ, false, pointer.getValue()); // get a reference to the process
        Psapi.GetModuleBaseNameW(process, null, buffer, maxTitleLength); // read the name of the process into buffer
        return Native.toString(buffer); // convert buffer to String
    }

    public static String getTopWindowTitle() {
        byte[] windowText = new byte[512];
        PointerType hwnd = User32.INSTANCE.GetForegroundWindow();
        User32.INSTANCE.GetWindowTextA(hwnd, windowText, 512);
        return Native.toString(windowText);
    }

    public static boolean isFullscreen() {
        Rectangle topWindow = getTopWindowRect();
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

        return topWindow.x == 0 && topWindow.y == 0 && topWindow.getWidth() == screen.getWidth() && topWindow.getHeight() == screen.getHeight();
    }

    public static Rectangle getTopWindowRect() {
        PointerType topWindow = User32.INSTANCE.GetForegroundWindow();
        return getWindowRect(topWindow);
    }

    public static Rectangle getWindowRect(PointerType hWnd) {
        Rectangle result = null;
        WinDef.RECT rect = new WinDef.RECT();
        boolean rectOK = User32.INSTANCE.GetWindowRect(hWnd, rect);
        if (rectOK) {
            int x = rect.left;
            int y = rect.top;
            int width = rect.right - rect.left;
            int height = rect.bottom - rect.top;
            result = new Rectangle(x, y, width, height);
        }

        return result;
    }

    public static boolean isWorkstationLocked() {
        return "LockApp.exe".equals(getTopWindowName());
    }

}

# Windows Component Group For Conductor
Component group that is used for controlling Windows with Conductor.

## Features
- Detect keystrokes.
- Show notifications.
- Get the current top window.
